&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEF VAR ii AS INT NO-UNDO.
DEF VAR logbid AS LOG NO-UNDO.

DEFINE VAR last_f AS INT NO-UNDO.
DEFINE VAR last_c AS INT NO-UNDO.
DEFINE VAR LAST_bl AS INT NO-UNDO.

DEFINE VARIABLE i AS INTEGER  NO-UNDO.
DEFINE VARIABLE ttfac AS INTEGER  NO-UNDO.
{hcombars.i}
{g-hprowinbase.i}

DEF VAR hcombars-f1 AS INT NO-UNDO.
DEF VAR FrmComBars-f1 AS HANDLE.

&SCOPED-DEFINE babandonner 101
&SCOPED-DEFINE bcreer 102

{Hgcoergo.i}
{hfileopen.i}
  
DEFINE TEMP-TABLE tps
FIELD n_produit AS INT
FIELD qte AS INT
FIELD prix AS DEC
FIELD tht AS DEC.
DEFINE VARIABLE som AS INTEGER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME br$1

/* Definitions for FRAME DEFAULT-FRAME                                  */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-1 RECT-2 f$date f$nof f$nocli b$client ~
f$nocde f$adr f$produit b$produit f$qte f$produitprx b$1 br$1 
&Scoped-Define DISPLAYED-OBJECTS f$date f$nof f$nocli f$nocde f$nomclient ~
f$adr f$no_bl f$produit f$nomproduit f$qte f$produitprx 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON b$1 
     LABEL "ajouter" 
     SIZE 12 BY 1.13.

DEFINE BUTTON b$client 
     LABEL "" 
     SIZE 4 BY 1.13.

DEFINE BUTTON b$produit 
     LABEL "" 
     SIZE 4 BY 1.13.

DEFINE VARIABLE f$adr AS CHARACTER FORMAT "X(256)":U 
     LABEL "Adresse" 
     VIEW-AS FILL-IN 
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE f$date AS DATE FORMAT "99/99/99":U 
     LABEL "Date facture" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE f$nocde AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N� Commande" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE f$nocli AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N� Client" 
     VIEW-AS FILL-IN 
     SIZE 13 BY 1 NO-UNDO.

DEFINE VARIABLE f$nof AS INTEGER FORMAT ">>>,>>>,>>9":U INITIAL 0 
     LABEL "N� Facture" 
     VIEW-AS FILL-IN 
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE f$nomclient AS CHARACTER FORMAT "X(256)":U 
     LABEL "" 
     VIEW-AS FILL-IN 
     SIZE 30.57 BY 1
     BGCOLOR 1 FGCOLOR 16  NO-UNDO.

DEFINE VARIABLE f$nomproduit AS CHARACTER FORMAT "X(256)":U 
     LABEL "" 
     VIEW-AS FILL-IN 
     SIZE 33.72 BY 1
     BGCOLOR 1 FGCOLOR 15  NO-UNDO.

DEFINE VARIABLE f$no_bl AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N� bL" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE f$produit AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "N� produit" 
     VIEW-AS FILL-IN 
     SIZE 20 BY 1 NO-UNDO.

DEFINE VARIABLE f$produitprx AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Prix de vente" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE f$qte AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Qte" 
     VIEW-AS FILL-IN 
     SIZE 15 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 104 BY 8.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 104 BY 6. 

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br$1 FOR 
      tps SCROLLING.
&ANALYZE-RESUME


/* Browse definitions                                                   */
DEFINE BROWSE br$1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br$1 C-Win _FREEFORM
 QUERY br$1 DISPLAY
      tps.n_produit FORMAT ">>>>>9"     COLUMN-LABEL "N�produit" WIDTH 15
      tps.qte      FORMAT ">>>>>9"      COLUMN-LABEL "Qte"       WIDTH 15
      tps.prix     FORMAT ">>>>>9"      COLUMN-LABEL "Prix"      WIDTH 15
      tps.tht     FORMAT ">>>>>9"      COLUMN-LABEL    "TOTAL produit"      WIDTH 15
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 104 BY 7.25 ROW-HEIGHT-CHARS 1.17 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME DEFAULT-FRAME
     f$date AT ROW 5 COL 22.43 COLON-ALIGNED WIDGET-ID 6
     f$nof AT ROW 5 COL 89.86 COLON-ALIGNED WIDGET-ID 34
     f$nocli AT ROW 7 COL 19 COLON-ALIGNED WIDGET-ID 8
     b$client AT ROW 7 COL 35 WIDGET-ID 32
     f$nocde AT ROW 7 COL 89.72 COLON-ALIGNED WIDGET-ID 36
     f$nomclient AT ROW 7.08 COL 38.43 COLON-ALIGNED WIDGET-ID 38
     f$adr AT ROW 9 COL 19 COLON-ALIGNED WIDGET-ID 10
     f$no_bl AT ROW 9 COL 91.57 COLON-ALIGNED WIDGET-ID 42
     f$produit AT ROW 13 COL 19 COLON-ALIGNED WIDGET-ID 14
     b$produit AT ROW 13 COL 47 WIDGET-ID 30
     f$nomproduit AT ROW 13.04 COL 50.43 COLON-ALIGNED WIDGET-ID 40
     f$qte AT ROW 15 COL 19 COLON-ALIGNED WIDGET-ID 16
     f$produitprx AT ROW 15 COL 57 COLON-ALIGNED WIDGET-ID 18
     b$1 AT ROW 15 COL 86.14 WIDGET-ID 22
     br$1 AT ROW 19 COL 6 WIDGET-ID 200
     "Produits" VIEW-AS TEXT
          SIZE 18 BY 1 AT ROW 18 COL 6 WIDGET-ID 28
          FGCOLOR 1 
     "   Ent�te facture" VIEW-AS TEXT
          SIZE 18 BY 1 AT ROW 3 COL 13 WIDGET-ID 4
          FGCOLOR 1 
     "  Ligne produit" VIEW-AS TEXT
          SIZE 18 BY 1 AT ROW 10.75 COL 13 WIDGET-ID 24
          FGCOLOR 1 
     RECT-1 AT ROW 3.5 COL 6 WIDGET-ID 2
     RECT-2 AT ROW 11.5 COL 6 WIDGET-ID 12
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 122 BY 25.96 WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "gestion des facteurs"
         HEIGHT             = 26.5
         WIDTH              = 126.43
         MAX-HEIGHT         = 34.25
         MAX-WIDTH          = 149.43
         VIRTUAL-HEIGHT     = 34.25
         VIRTUAL-WIDTH      = 149.43
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME                                                           */
/* BROWSE-TAB br$1 b$1 DEFAULT-FRAME */
/* SETTINGS FOR FILL-IN f$nomclient IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$nomproduit IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN f$no_bl IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* gestion des facteurs */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* gestion des facteurs */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME DEFAULT-FRAME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL DEFAULT-FRAME C-Win
ON GO OF FRAME DEFAULT-FRAME
DO:

  END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b$1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b$1 C-Win
ON CHOOSE OF b$1 IN FRAME DEFAULT-FRAME /* ajouter */
DO:
    ASSIGN f$produit f$qte f$produitprx.
  CREATE tps.
  
      ASSIGN 
      tps.n_produit = f$produit
      tps.qte = f$qte
      tps.prix = f$produitprx
      tps.tht = INT (f$produitprx) * INT (f$qte)
      ttfac = ttfac + (INT (f$produitprx) * INT (f$qte)).
      OPEN QUERY br$1 FOR EACH tps.
             f$produit:SCREEN-VALUE = "".
             f$nomproduit:SCREEN-VALUE = "".
             f$qte:SCREEN-VALUE = "".
             f$produitprx:SCREEN-VALUE = "".
       
        
        

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b$client
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b$client C-Win
ON CHOOSE OF b$client IN FRAME DEFAULT-FRAME
DO:
  /*DEFINE VARIABLE cod AS INT  NO-UNDO.
  DEFINE VARIABLE rid AS ROWID      NO-UNDO.
  RUN dynrechex("recherche client", /*Nom fenetre*/
                "GCO.client", /*la table*/
                "", /*Where*/
                "cod_cli�CODE client|nom_cli�nom client", /*colonnnes affich�s*/
                "cod_cli", /*Champs a recuperer*/
                OUTPUT cod, OUTPUT rid,
                "PLEINECRAN", "", ? ).
  f$nocli:SCREEN-VALUE = STRING (cod).*/

    def var parlib      as C no-undo.
    def var parcod      as C no-undo.
    run reccli_b ("","",input-output parcod, output parlib).
    if parcod <> "" then do:
        assign f$nomclient = parlib.
        /*if rscli-pro:screen-val = "C" then do:*/
            f$nocli = INTEGER(parcod).
            disp f$nocli with frame DEFAULT-FRAME.
            /*RUN actu-bouton-creer.  */    /* $A40401 */
        /*end.*/
        display f$nomclient with frame DEFAULT-FRAME.
        /*RUN openquery (NO).*/
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME b$produit
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL b$produit C-Win
ON CHOOSE OF b$produit IN FRAME DEFAULT-FRAME
DO:
 /* DEFINE VARIABLE cod AS INT  NO-UNDO.
  DEFINE VARIABLE rid AS ROWID      NO-UNDO.
  RUN dynrechex("recherche produit", /*Nom fenetre*/
                "GCO.produit", /*la table*/
                "", /*Where*/
                "cod_pro�CODE produit|nom_pro�nom produit", /*colonnnes affich�s*/
                "cod_pro", /*Champs a recuperer*/
                OUTPUT cod, OUTPUT rid,
                "PLEINECRAN", "", ? ).
  f$produit:SCREEN-VALUE = STRING (cod).*/
    def var parli      as C no-undo.
    def var parco      as C no-undo.
def var rrecha      as c no-undo.
    DEF VAR n_dep AS INT no-undo. /* N� D�p�t choisi */

           run recpro_b ("9",n_dep,"","","","",rrecha,input-output parco, output parli,output logbid).

         if parco <> "" then do:
           assign f$nomproduit = parli.
           /*if rscli-pro:screen-val = "C" then do:*/
            f$produit = INTEGER(parco).
            disp f$produit with frame DEFAULT-FRAME.
            /*RUN actu-bouton-creer.  */    /* $A40401 */
           /*end.*/
           display f$nomproduit with frame DEFAULT-FRAME.
           /*RUN openquery (NO).*/
      END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME br$1
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */
/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */

/* Gestion des lignes/colonnes des fichiers interface */
ON WINDOW-CLOSE OF C-Win DO:
  RUN choose-babandonner.
  RETURN NO-APPLY.
END.

ON alt-cursor-up OF FRAME default-frame ANYWHERE DO:
    DEF VAR logbid AS LOG.
    logbid = CBFocusItem (hcombars-f1,2,{&babandonner}).
END.


ON CLOSE OF THIS-PROCEDURE DO:
    RUN putini.
    IF VALID-HANDLE(hComBars) THEN APPLY "close" TO hComBars.
    IF VALID-HANDLE (HndGcoErg) THEN APPLY "CLOSE" TO HndGcoErg.

    IF VALID-HANDLE (Hfileopen) THEN APPLY "close" TO hFileOpen.
    IF VALID-HANDLE(C-Win) THEN DELETE WIDGET C-Win.
    IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END.
  
ON RETURN OF c-win ANYWHERE DO:
    IF CAN-DO("fill-in,toggle-box,radio-set,combo-box",SELF:TYPE) THEN DO:
      APPLY "tab" TO SELF.
      RETURN NO-APPLY.
    END.
    ELSE IF SELF:TYPE="editor" THEN LOGBID=SELF:INSERT-STRING("~n").
    ELSE IF SELF:TYPE="button" THEN APPLY "choose" TO SELF.
    ELSE IF CAN-DO("browse,selection-list",SELF:TYPE) THEN APPLY "default-action" TO SELF.
END.


ON f6 OF FRAME default-frame ANYWHERE
    RUN choose-bcreer.

ON LEAVE OF c-win ANYWHERE DO:
    IF SELF:bgcolor = 31 THEN SELF:bgcolor = 16.
END.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:

  RUN proc-init.
  SESSION:SET-WAIT-STATE ("").
  VIEW c-win.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-babandonner C-Win 
PROCEDURE choose-babandonner :
DO WITH FRAME DEFAULT-FRAME : 

        APPLY "close" TO THIS-PROCEDURE.

    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE choose-bcreer C-Win 
PROCEDURE choose-bcreer :
DO WITH FRAME DEFAULT-FRAME : 
  
    
 DEFINE VARIABLE reps AS LOGICAL    NO-UNDO.
    
MESSAGE "VOULEZ VOUS Ajouter un Enregistrement ? "
            VIEW-AS ALERT-BOX INFO BUTTONS YES-NO UPDATE reps.
 IF f$nocli <> 0  THEN DO:
        
                      IF reps THEN  DO:
                       i = 0.
                      ASSIGN f$nof f$nocli  f$adr f$nocde f$date f$no_bl.
                      CREATE histoent.
               
                      ASSIGN  histoent.no_fact   = f$nof
                              histoent.cod_cf   = f$nocli
                              histoent.typ_mvt  = "C"
                              histoent.dat_cde  = f$date
                              histoent.adr_fac  = f$adr
                              histoent.no_cde   = f$nocde
                              histoent.dat_cde  = f$date
                              histoent.no_bl    = f$no_bl
                              histoent.mt_ht    = ttfac
                              histoent.mt_cde   = ttfac * 1.2 .

            

                      FOR EACH tps NO-LOCK :
                      CREATE histolig.

                      i = i + 25.

                      ASSIGN
                      histolig.no_cde   = f$nocde
                      histolig.typ_mvt  = "C"
                      histolig.no_bl    = f$no_bl
                      histolig.cod_pro  = tps.n_produit
                      histolig.qte      = tps.qte
                      histolig.px_vte   = tps.prix
                      histolig.cod_pro  = tps.tht
                      histolig.NO_ligne = i.


                       END.

                    END.
           MESSAGE "LE TRAITEMENT TERMINE AVEC SUCCES !"
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.

   FOR LAST histoent NO-LOCK WHERE histoent.typ_mvt = "C"  BY histoent.no_fact:
        IF histoent.no_fact > last_c  THEN
            last_f = histoent.no_fact.
    END.
    last_c = NEXT-VALUE(compteur_commande_client).
    LAST_bl = NEXT-VALUE(compteur_bl).
            /*ASSIGN f$f f$com.*/
            f$nof = INT (last_f + 1).
            f$nocde = INT (last_c).
            f$no_bl = INT (LAST_bl).
            DISP  f$nof f$nocde f$no_bl with frame DEFAULT-FRAME.
         
            END.
            f$nocli:SCREEN-VALUE = "".
            f$nomclient:SCREEN-VALUE = "".
            f$adr:SCREEN-VALUE = "".
            f$date:SCREEN-VALUE = "".
        RUN proc-init.



END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsCreation C-Win 
PROCEDURE CombarsCreation :
RUN gcoErgo PERSISTENT SET HndGcoErg ({&WINDOW-NAME}:HANDLE).
    SET-TITLE (FRAME DEFAULT-FRAME:HANDLE, {&WINDOW-NAME}:TITLE). 
    RUN combars PERSISTENT SET hcombars.
    hcombars-f1 = cbocxadd (FRAME DEFAULT-FRAME:HANDLE, OUTPUT FrmComBars-f1).
    CBSetSize(hcombars-f1, 1.04,  28.26, 1.15,FRAME DEFAULT-FRAME:WIDTH - 27.66).
    CBAddBar(hcombars-f1, 'Barre outils 1', {&xtpBarTop}, YES, NO).
    CBShowMenu(hcombars-f1, NO).
    CBToolBarAccelTips (hcombars-f1, NO).

    CBLoadIcon(hcombars-f1, {&babandonner},StdMedia + "\StdMedia.icl",1). 
    CBLoadIcon(hcombars-f1, {&bcreer},StdMedia + "\StdMedia.icl",36).


    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&babandonner}, Translate ('&Abandonner') , YES).
    CBSetItemStyle(hcombars-f1, 2, {&babandonner}, {&xtpButtonIconAndCaption}).
    CBAddItem(hcombars-f1, 2, ?, {&xtpControlButton}, {&bcreer}, Translate ('&Creer' + ' (F6)') , NO).
    CBSetItemStyle(hcombars-f1, 2, {&bcreer}, {&xtpButtonIconAndCaption}).
 
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CombarsExecute C-Win 
PROCEDURE CombarsExecute :
DEF INPUT PARAM pnumcombar AS INT.
    DEF INPUT PARAM pbutton AS INT.

    IF pnumcombar = hcombars-f1 THEN DO:
        CASE pbutton :
            WHEN {&babandonner} THEN RUN choose-babandonner.
            WHEN {&bcreer} THEN RUN choose-bcreer.
        END CASE.
    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY f$date f$nof f$nocli f$nocde f$nomclient f$adr f$no_bl f$produit 
          f$nomproduit f$qte f$produitprx 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE RECT-1 RECT-2 f$date f$nof f$nocli b$client f$nocde f$adr f$produit 
         b$produit f$qte f$produitprx b$1 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE getini C-Win 
PROCEDURE getini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE proc-init C-Win 
PROCEDURE proc-init :
DO WITH FRAME default-frame:
     ENABLE ALL WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
     DISABLE f$nomproduit f$nomclient f$no_bl f$nof f$nocde .



        last_f  = 0.
     FOR LAST entetcli  NO-LOCK BY entetcli.no_fact :
        ASSIGN
            last_f = entetcli.no_fact.
    END.
    FOR LAST histoent NO-LOCK WHERE histoent.typ_mvt = "C"  BY histoent.no_fact:
        IF histoent.no_fact > last_c  THEN
            last_f = histoent.no_fact.
    END.
    last_c = NEXT-VALUE(compteur_commande_client).
    LAST_bl = NEXT-VALUE(compteur_bl).
            /*ASSIGN f$f f$com.*/
            f$nof = INT (last_f + 1).
            f$nocde = INT (last_c).
            f$no_bl = INT (LAST_bl).
            DISP  f$nof f$nocde f$no_bl with frame DEFAULT-FRAME.
        RUN getini.


        RUN combarscreation.


    END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE putini C-Win 
PROCEDURE putini :
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

